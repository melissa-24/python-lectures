
from collections import deque

class Graph:

    def __init__(self):
        # vertex_id --> set of neighbors
        self.vertices = {}

    def __repr__(self):
        return str(self.vertices)

    def add_vertex(self, vertex_id):
        self.vertices[vertex_id] = set()

    def remove_vertex(self, vertex_id):
        if vertex_id not in self.vertices:
            print('Trying to remove non-existent vertex')
            return
        self.vertices.pop(vertex_id)
        print(f'after pop{self.vertices}')
        for remaining_vertex in self.vertices:
            self.vertices[remaining_vertex].discard(vertex_id)

    def remove_edge(self, from_vertex_id, to_vertex_id):
        if from_vertex_id not in self.vertices or to_vertex_id not in self. vertices:
            print('Trying to remove non-existent vertex')
            return
        self.vertices[from_vertex_id].discard(to_vertex_id)

    def add_edge(self, from_vertex_id, to_vertex_id):
        if from_vertex_id not in self.vertices or to_vertex_id not in self.vertices:
            print('trying to add edge to non-existing node')
            return
        self.vertices[from_vertex_id].add(to_vertex_id)

    def get_neighbors(self, vertex_id):
        return self.vertices[vertex_id]

    def dft(self, starting_vertex):
        visited = set()
        stack = deque()
        stack.append(starting_vertex)
        while len(stack) > 0:
            currNode = stack.pop()
            if currNode not in visited:
                visited.add(currNode)
                print(currNode)
                for neighbor in self.vertices[currNode]:
                    stack.append(neighbor)

    def dfs(self, starting_vertex, goal_vertex):
        visited = set()
        stack = deque()
        # Push the current path your're on onto the stack, instead of just a single vertex
        stack.append([starting_vertex])
        while len(stack) > 0:
            currNode = stack.pop()
            currNode= currPath[-1] # the current node you're on is the last node in teh path
            if currNode == goal_vertex:
                return currPath
            if currNode not in visited:
                visited.add(currNode)
                for neighbor in self.vertices[currNode]:
                    newPath = list(currPath) # make a copy of hte current path
                    newPath.append(neighbor)
                    stack.append(newPath)

    def bft(self, starting_vertex):
        visited = set()
        queue = deque()
        queue.append(starting_vertex)
        while len(queue) > 0:
            currNode = queue.popleft()
            if currNode not in visited:
                visited.add(currNode)
                print(currNode)
                for neighbor in self.vertices[currNode]:
                    queue.append(neighbor)

def dft_recursive(self, starting_vertex):
    visited = set()
    self.dft_recursive_helper(starting_vertex, visited)

def dtf_recursive_helper(self, curr_vertex, visited):
    visited.add(curr_vertex)
    print(curr_vertex)
    for neighbor in self.vertices[curr_vertex]:
        if neighbor not in visited:
            # recursive case
            self.dft_recursive_helper(neighbor, visited)

def dfs_recursive(self, starting_vertex, goal_vertex):
    visited = set()
    return self.dfs_recursive_helper([starting_vertex], visited, goal_vertex)

# Return the path to goal_vertex if it exists.  If it doesn't, return an empty array
def dfs_recursive_helper(self, curr_path, visited, goal_vertex):
    curr_vertex = curr_path[-1]
    # Base-case if curr vertex is the goal vertex, return its path
    if curr_vertex == goal_vertex:
        return curr_path
    visited.add(curr_vertex)
    for neighbor in self.vertices[curr_vertex]:
        if neighbor not in visited:
            newPath = list(curr_path)
            newPath.append(neighbor)
            # Recursive case - keep traversing the graph and visit the neighbor next
            self.dfs_recursive_helper(newPath, visited, goal_vertex)
            if len(res) > 0:
                return res
    # Base-case return empty array if goal vertex not found
    return []


graph = Graph()
graph.add_vertex(1)
graph.add_vertex(2)
graph.add_vertex(3)
graph.add_vertex(4)

graph.add_edge(1, 2)
graph.add_edge(1, 3)
graph.add_edge(2, 4)
graph.add_edge(3, 4)
graph.add_edge(4, 1)





