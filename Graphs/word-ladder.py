from collections import deque

"""

Given two words (begin_word and end_word), and a dictionary's word list, return the shortest transformation sequence from begin_word to end_word, such that:

Only 1 letter can be changed at a time.
Each tranformed word must exist in teh word list. Note that begin_word is not a tranformed word.

Note:
Return [] if there is no such transformation sequence.
All words contain only lowercase alphabetic characters.
You may assume no duplicated in the word list.
You may assume begin_word and end_word are non-empty  and are not the same.

Sample:

beginWord = “hit”
endWord = “cog”
wordList = [“hot”, “dot”, “dog”, “lot”, “log”, “cog”]
[“hit”, “hot”, “dot”, “dog”, “cog”]

Plan
1. Translate into graph terminology
vertex = words
edges = A possible 1 letter transformations from a word to another word
weights = N/A
path = 1 letter transformations from the begin_word to end_word

2. Build your graph if needed
 - creating all possible transformations in an adjacency list would be too expensive
 - we can come up with how to find out the next vertex by determining if it's even a valid vertex to visit
 - we should visit a vertex if that vertex is in the wordlist

3. Traverse your graph
 - since we want the shortest path, we want to use BFS
 - start from the begin_word and generate word transformations from it
 - enqueue transformations that are in the word list, ignore the rest
 - once we find end_word, return the path it took to get to that node

"""
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

def findLadders(beginWord, endWord, wordList):
    words = set(wordList)
    visited = set()
    queue = deque()
    queue.append([beginWord])
    while len(queue) > 0:
        currPath = queue.popleft()
        currWord = currPath[-1]
        if currWord in visited:
            continue
        visited.add(currWord)
        if currWord == endWord:
            return currPath
        for i in range(len(currWord)):
            for letter in alphabet:
                transformedWord = currWord[:i] + letter + currWord[i + 1:]
                if transformedWord in words:
                    newPath = list(currPath)
                    newPath.append(transformedWord)
                    queue.append(newPath)
    return []

print(findLadders("hit", "cog", ["hot","dot","dog","lot","log","cog"]))
print(findLadders("hit", "cog", ["hot","dot","dog","lot","log"]))
