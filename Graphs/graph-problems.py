# • Translate the problem into graph terminology
#     • What are the vertices, edges, weights (if needed)?
# • Build your graph
#     • Do you even need to build a graph? Should you use an adjacency matrix/list?
# • Traverse your graph
#     • Should you use BFS/DFS? Do you need an auxiliary data structure?

# https://leetcode.com/problems/destination-city/

# Not runable code the way this is written.

"""
Understand

paths = [["London","New York"],["New York","Lima"],["Lima","Sao Paulo"]]

Output "Sao Paulo"
    
Plan

1. translate into graph terminology
vertex = cities 
edges = paths
weights = N/A

2. build your graph
use an adjacency list
- create a dictionary, with each city as a key, and the value is a set of its neighbors

3. traverse the graph
BF/DF? does it matter? NO


"""
from collections import deque

class Solution:
    def destCity(self, paths: List[List[str]]) -> str:
        if len(paths) == 0:
            return ''
        graph = self.createGraph(paths)
        stack = deque()
        stack.append(paths[0][0])
        visited = set()
        while len(stack) > 0:
            curr = stack.pop()
            visited.add(curr)
            if curr not in graph:
                return curr
            else:
                for neighbor in graph[curr]:
                    if neighbor not in visited:
                        stack.append(neighbor)
        return ''

    def createGraph(self, paths):
        graph = {}
        for edge in paths:
            origin, destination = edge[0], edge[1]
            if origin in graph:
                graph[origin].add(destination)
            else:
                graph[origin] = { destination }
        return graph