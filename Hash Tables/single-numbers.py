# https://leetcode.com/problems/single-number/

# This code will not run on local.

"""
Understand

[2, 2, 1] --> 1
[1] --> 1
[2, 1, 2] --> 1

Plan

Use a dictionary to keep track of how many times an item exists [element: # of times it appears]
Go through the dictionary and output the item that appears once

Runtime: O(n)
Space: O(n)
"""
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        dictionary = {}

        for n in nums:
            if n in dictionary:
                dictionary[n] += 1
            else:
                dictionary[n] = 1

        for (key, value) in dictionary.items():
            if value == 1:
                return key

        return -1