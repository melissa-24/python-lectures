
# https://leetcode.com/problems/jewels-and-stones/

# Code will not run on this way

"""
Understand
1. J = "aA", S = "aAAbbbb"
output 3

2. J = "z", S = "ZZ"
output 0

3. J = "z" S = ""
output 0

Plan
brute-force
for each char s in S, see if it's in J
if it is, then increment result else don't increment it

better approach
convert J into a set j
for each char s in S
   if s is in j then increment result
return result
"""

class Solution:
    # J = "aA", S = "aAAbbbb"
    def numJewelsInStones(self, J: str, S: str) -> int:
        j = set(list(J))
        numJewels = 0
        for s in S:
            if s in j:
                numJewels += 1
        return numJewels 