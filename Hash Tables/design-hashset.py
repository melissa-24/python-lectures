# https://leetcode.com/problems/design-hashset/

# This code will not run on local

"""
Understand

mySet = MyHashSet()
mySet.add(1) // {1}
mySet.add(2) // {1, 2}
mySet.add(1) // {1, 2}

mySet.contains(1) // True
mySet.remove(1)
mySet.contains(1) // False

Plan

Implement a hash table to implement get/add/remove
Use a popular hashing algorithm to map elements into buckets
Solve collisions using chaining
For simplicity we just need to initialize an array of size 10001 and not need to resize

Runtime: O(1)
Space: O(1)
"""

from collections import deque

class MyHashSet:
    def __init__(self):
        self.arr = [None] * 10001

    def hashIndex(self, key):
        return hash(key) % len(self.arr)

    def add(self, key: int) -> None:
        hashIndex = self.hashIndex(key)
        if self.arr[hashIndex] is None:
            newList = deque()
            newList.append(key)
            self.arr[hashIndex] = newList
        elif key not in self.arr[hashIndex]:
            self.arr[hashIndex].append(key)

    def remove(self, key: int) -> None:
        hashIndex = self.hashIndex(key)
        if self.arr[hashIndex] is not None:
            try:
                self.arr[hashIndex].remove(key)
            except:
                pass

    def contains(self, key: int) -> bool:
        hashIndex = self.hashIndex(key)
        if self.arr[hashIndex] is not None:
            return key in self.arr[hashIndex]
        return False